
function search_id(array, id) {

    if(array === undefined || array.length === 0 || id > array.length || id === undefined) {
        return []
    }
    for(let i = 0; i < array.length; i++) {
        let objId = array[i]["id"];
        if(objId === id) {
               return array[i]
        }
    }
    
}

module.exports = search_id;