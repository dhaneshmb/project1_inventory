
function specificVehicleMake(array, vehicle1, vehicle2) {
    if(array === undefined || array.length === 0) {
      return []
    }
    if(vehicle1 === undefined && vehicle2 === undefined) {
      return []
    }
    let specificVehicleArray = [];
    for(let i = 0; i < array.length; i++) {
      let vehicle_make = array[i]["car_make"];
      if(vehicle_make === vehicle1 || vehicle_make === vehicle2) {
        specificVehicleArray.push(array[i]);
      }
    }
    return JSON.stringify(specificVehicleArray)
  }

  module.exports = specificVehicleMake;