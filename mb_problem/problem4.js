
function vehicleYear(array) {
  if(array !== undefined && array.length !== 0){
    let year_array = [];
    for(let i = 0; i < array.length; i++){
      let year = array[i]["car_year"];
      year_array.push(year);
    }
    return year_array
  }
  return []
}

  module.exports = vehicleYear;