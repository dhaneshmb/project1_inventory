

function alphabetically_sorted_model(array) {

    
    if(array !== undefined && array.length !== 0) {
       
       let model_array = [];
       for(let i = 0; i < array.length; i++) {
         let model = array[i]["car_model"];
         model_array.push(model);     
       }
 
       let sorted_array = model_array.sort((a, b) => a.localeCompare(b));
       return sorted_array;
    }
    return []
 
 }

 module.exports = alphabetically_sorted_model;