
function olderVehicle(array, yearsArray) {
    if(array === undefined || array.length === 0 || yearsArray === undefined || yearsArray.length === 0) {
      return []
    }
    let olderVehicleArray = [];
    for(let i = 0; i < array.length; i++) {
        if(yearsArray[i] < 2000) {
            let vehicleObj = array[i];
            olderVehicleArray.push(vehicleObj);
        }
    }
    return olderVehicleArray
  }

  module.exports = olderVehicle